package net.hermlon.picturehealer.main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.imageio.ImageIO;

public class HealerTest {

	private PictureHealer healer;
	private File inputTrain = new File("/home/andre/code/gustav/Bilder/Cover/Puzzle.png");
	private File inputTrain2 = new File("/home/andre/code/gustav/Bilder/Covered/cat2_puzzle.png");
	private File outputTrain = new File("/home/andre/code/gustav/Bilder/Input/cat2.png");
	private File outputDir = new File("/home/andre/code/gustav/Bilder/output_new");
	
	public HealerTest() {
		healer = new PictureHealer();
		healer.setGenerations(200);
		healer.train(inputTrain, inputTrain2, outputTrain);
		
		
		File f1 = new File("/home/andre/code/gustav/Bilder/Cover/Schrift.png");
		File f2 = new File("/home/andre/code/gustav/Bilder/Covered/cat2_schrift.png");
		
		BufferedImage[] img1 = {healer.pass(f1, f2)};
		savePictures(img1, "test");
	}
	
	private void savePictures(BufferedImage[] pictures, String tag) {
		for(int i = 0; i < pictures.length; i ++) {
			if(i % 10 == 0) {
				try {
					ImageIO.write(pictures[i], "png", new File(outputDir, tag + "_" + getTimestamp() + "_gen_" + Integer.toString(i) + ".png"));
				}
				catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private String getTimestamp() {
		GregorianCalendar g = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmss");
		return sdf.format(g.getTime());
	}
}
