package net.hermlon.picturehealer.main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.hermlon.picturehealer.neuronalnetwork.NeuronalNetworkTrainer;

public class PictureHealer {
	
	private NeuronalNetworkTrainer networkTrainer;
	private int generations = 10;
	private int generation = 0;
	
	public PictureHealer() {
		networkTrainer = new NeuronalNetworkTrainer(2); 
	}
	
	public BufferedImage[] train(File input, File input2, File expectedOutput) {
		try {
			BufferedImage i = ImageIO.read(input);
			BufferedImage i2 = ImageIO.read(input2);
			BufferedImage o = ImageIO.read(expectedOutput);
			return trainPictures(i, i2, o);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public BufferedImage pass(File input, File input2) {
		try {
			BufferedImage i = ImageIO.read(input);
			BufferedImage i2 = ImageIO.read(input2);
			BufferedImage[] image = {i, i2};
			return networkTrainer.passNetwork(image);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private BufferedImage[] trainPictures(BufferedImage input, BufferedImage input2, BufferedImage expectedoutput) {
		BufferedImage[] i = {input, input2};
		BufferedImage[] output = new BufferedImage[generations];
		for(generation = 0; generation < generations; generation ++) {
			double p = 100d / generations * generation;
			System.out.println((int) p + "%");
			output[generation] = networkTrainer.trainNetwork(i, expectedoutput);
		}
		return output;
	}

	public int getGenerations() {
		return generations;
	}

	public void setGenerations(int generations) {
		this.generations = generations;
	}
}
