package net.hermlon.picturefilter.neuronalnetwork;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class NeuronalNetworkTrainer {
	
	private double learningrate = 0.05;
	private NeuronalNetwork neuronalNetwork;
	
	public NeuronalNetworkTrainer(int inputimages) {
		neuronalNetwork = new NeuronalNetwork(inputimages * 3 * 3 * 3, 8, 3);
	}
	
	public BufferedImage trainNetwork(BufferedImage[] input, BufferedImage expectedResult) {
		neuronalNetwork.setLearningrate(learningrate);
		double[] pixels = new double[(input[0].getWidth() - 2) * (input[0].getHeight() - 2) * 3];
		int pos = 0;
		for(int i = 0; i <= input[0].getHeight() - 3; i ++) {
			for(int j = 0; j <= input[0].getWidth() - 3; j ++) {
				double[] r = neuronalNetwork.train(toNetworknumber(getPixels(input, j, i)), toNetworknumber(getExpectedResult(expectedResult, j, i)));
				double result[] = toRGBNumber(r);
				pixels[pos] = result[1];
				pixels[pos + 1] = result[2];
				pixels[pos + 2] = result[3];
				pos += 3;
			}
		}
		return toImage(pixels, input[0].getWidth() - 2, input[0].getHeight() - 2);
	}
	
	public BufferedImage passNetwork(BufferedImage[] input) {
		neuronalNetwork.setLearningrate(learningrate);
		double[] pixels = new double[(input[0].getWidth() - 2) * (input[0].getHeight() - 2) * 3];
		int pos = 0;
		for(int i = 0; i <= input[0].getHeight() - 3; i ++) {
			for(int j = 0; j <= input[0].getWidth() - 3; j ++) {
				double[] r = neuronalNetwork.train(toNetworknumber(getPixels(input, j, i)), null);
				double result[] = toRGBNumber(r);
				pixels[pos] = result[1];
				pixels[pos + 1] = result[2];
				pixels[pos + 2] = result[3];
				pos += 3;
			}
		}
		return toImage(pixels, input[0].getWidth() - 2, input[0].getHeight() - 2);
	}
	
	private BufferedImage toImage(double[] pixels, int width, int height) {
		Color[] colors = new Color[pixels.length / 3];
		int pos = 0;
		for(int i = 0; i < colors.length; i ++) {
			colors[i] = new Color((int) pixels[pos], (int) pixels[pos + 1], (int) pixels[pos + 2]);
			pos += 3;
		}
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		int n = 0;
		for(int i = 0; i < height; i ++) {
			for(int j = 0; j < width; j ++) {
				image.setRGB(j, i, colors[n].getRGB());
				n ++;
			}
		}
		return image;
	}
	
	private double[] getExpectedResult(BufferedImage image, int x, int y) {
		double pixels[] = new double[3];
		pixels[0] = new Color(image.getRGB(x + 1, y + 1)).getRed();
		pixels[1] = new Color(image.getRGB(x + 1, y + 1)).getGreen();
		pixels[2] = new Color(image.getRGB(x + 1, y + 1)).getBlue();
		return pixels;
	}
	
	private double[] getPixels(BufferedImage[] images, int x, int y) {
		double[] pixels = new double[images.length * 3 * 3 * 3];
		int pos = 0;
		for(int t = 0; t < images.length; t ++) {
			for(int i = 0; i < 3; i ++) {
				for(int j = 0; j < 3; j ++) {
					pixels[pos] = new Color(images[t].getRGB(x + i, y + j)).getRed();
					pos ++;
					pixels[pos] = new Color(images[t].getRGB(x + i, y + j)).getGreen();
					pos ++;
					pixels[pos] = new Color(images[t].getRGB(x + i, y + j)).getBlue();
					pos ++;
				}
			}
		}
		return pixels;
	}
	
	private double[] toNetworknumber(double[] colors) {
		double[] pixels = new double[colors.length];
		for(int i = 0; i < pixels.length; i ++) {
			pixels[i] = colors[i] / 255;
		}
		return pixels;
	}
	
	private double[] toRGBNumber(double[] numbers) {
		double[] colors = new double[numbers.length];
		for(int i = 0; i < colors.length; i ++) {
			colors[i] = numbers[i] * 255;
		}
		return colors;
	}

	public double getLearningrate() {
		return learningrate;
	}

	public void setLearningrate(double learningrate) {
		this.learningrate = learningrate;
	}
}
