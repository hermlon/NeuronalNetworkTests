package net.hermlon.picturefilter.neuronalnetwork;

import java.util.Random;

public class NeuronalNetwork {

	private int inputNeurons = 2;
	private int hiddenNeurons = 2;
	private int outputNeurons = 1;
	
	private double[] input;
	private double[] hidden;
	private double[] output;
	private double[][] hiddenWeights;
	private double[][] outputWeights;
	private double learningrate = 0.05;
	private static final Random random = new Random();
	
	public NeuronalNetwork(int inputNeurons, int hiddenNeurons, int outputNeurons) {
		this.inputNeurons = inputNeurons;
		this.hiddenNeurons = hiddenNeurons;
		this.outputNeurons = outputNeurons;
		hiddenWeights = new double[hiddenNeurons + 1][inputNeurons + 1];
		outputWeights = new double[outputNeurons + 1][hiddenNeurons + 1];
		randomWeights();
	}
	
	public double[] train(double[] input, double[] expectedOutput) {
		double[] output = startNetwork(input);
		if(expectedOutput != null) {
			backpropagation(expectedOutput);
		}
		return output;
	}
	
	public void setWeights(double[][] hiddenWeight, double[][] outputWeights) {
		this.hiddenWeights = hiddenWeight;
		this.outputWeights = outputWeights;
	}
	
	public double[][] getHiddenWeights() {
		return hiddenWeights;
	}
	
	public double[][] getOutputWeights() {
		return outputWeights;
	}
	
	private void randomWeights() {
		for(int i = 1; i <= hiddenNeurons; i ++) {
			for(int e = 0; e <= inputNeurons; e ++) {
				hiddenWeights[i][e] = getRandomWeight();
			}
		}
		for(int i = 1; i <= outputNeurons; i ++) {
			for(int e = 0; e <= hiddenNeurons; e ++) {
				outputWeights[i][e] = getRandomWeight();
			}
		}
	}
	
	private double getRandomWeight() {
		return (double) random.nextDouble() - 0.5;
	}
	
	private double[] startNetwork(double[] networkInput) {
		
		input = new double[inputNeurons + 1];
		hidden = new double[hiddenNeurons + 1];
		output = new double[outputNeurons + 1];
		output[0] = 1;
		hidden[0] = 1;
		input[0] = 1;
		
		for(int i = 0; i < networkInput.length; i ++) {
			input[i + 1] = networkInput[i];
		}
		
		
		for(int i = 1; i <= hiddenNeurons; i ++) {
			hidden[i] = 0.0;
			for(int e = 0; e <= inputNeurons; e ++) {
				hidden[i] += (input[e] * hiddenWeights[i][e]);
			}
			hidden[i] = sigmoide(hidden[i]);
		}
		for(int i = 1; i <= outputNeurons; i ++) {
			output[i] = 0.0;
			for(int e = 0; e <= hiddenNeurons; e ++) {
				output[i] += (hidden[e] * outputWeights[i][e]);
			}
			output[i] = sigmoide(output[i]);
		}
		return output;
	}
	
	private double sigmoide(double sum) {
		return (double) (1.0 / (1.0 + Math.exp(- sum)));
	}
	
	private void backpropagation(double[] desiredOutput) {
        double[] errorL2 = new double[outputNeurons + 1];
        double[] errorL1 = new double[hiddenNeurons + 1];
        double Esum = 0.0;

        for (int i = 1; i <= outputNeurons; i++) {
            errorL2[i] = output[i] * (1.0 - output[i]) * (desiredOutput[i - 1] - output[i]);
        }

        for (int i = 0; i <= hiddenNeurons; i++) {
            for (int j = 1; j <= outputNeurons; j++) {
                Esum += outputWeights[j][i] * errorL2[j];
            }

            errorL1[i] = hidden[i] * (1.0 - hidden[i]) * Esum;
            Esum = 0.0;
        }

        for (int j = 1; j <= outputNeurons; j++) {
            for (int i = 0; i <= hiddenNeurons; i++) {
                outputWeights[j][i] += learningrate * errorL2[j] * hidden[i];
            }
        }

        for (int j = 1; j <= hiddenNeurons; j++) {
            for (int i = 0; i <= inputNeurons; i++) {
                hiddenWeights[j][i] += learningrate * errorL1[j] * input[i];
            }
        }
    }

	public double getLearningrate() {
		return learningrate;
	}

	public void setLearningrate(double learningrate) {
		this.learningrate = learningrate;
	}
}
