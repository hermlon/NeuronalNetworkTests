package net.hermlon.picturefilter.neuronalnetwork;

import java.util.Random;

public class NeuronalNetworkMoreLayers {

	private int inputNeurons = 2;
	private int[] hiddenNeurons = {2};
	private int outputNeurons = 1;
	
	private double[] input;
	private double[][] hidden;
	private double[] output;
	private double[][][] hiddenWeights;
	private double[][] outputWeights;
	private double learningrate = 0.05;
	private static final Random random = new Random();
	
	public NeuronalNetworkMoreLayers(int inputNeurons, int[] hiddenNeurons, int outputNeurons) {
		this.inputNeurons = inputNeurons;
		this.hiddenNeurons = hiddenNeurons;
		this.outputNeurons = outputNeurons;
		hiddenWeights = new double[hiddenNeurons.length][getMaxHiddenNeurons() + 1][getMaxWeights() + 1];
		outputWeights = new double[outputNeurons + 1][getMaxHiddenNeurons() + 1];
		randomWeights();
	}
	
	private int getMaxHiddenNeurons() {
		int i = 0;
		for(int e = 0; e < hiddenNeurons.length; e ++) {
			if(hiddenNeurons[e] > i) {
				i = hiddenNeurons[e];
			}
		}
		return i;
	}
	
	private int getMaxWeights() {
		int maxhidden = getMaxHiddenNeurons();
		if(inputNeurons > maxhidden) {
			return inputNeurons * inputNeurons;
		}
		else {
			return maxhidden * maxhidden;
		}
	}
	
	public double[] train(double[] input, double[] expectedOutput) {
		double[] output = startNetwork(input);
		if(expectedOutput != null) {
			backpropagation(expectedOutput);
		}
		return output;
	}
	
	private void randomWeights() {
		for(int i = 0; i < hidden.length; i ++) {
			for(int e = 1; e <= hidden[0].length; e ++) {
				for(int j = 0; j < hiddenWeights[0][0].length; j ++) {
					hiddenWeights[i][e][j] = getRandomWeight();
				}
			}
		}
		for(int i = 1; i <= outputNeurons; i ++) {
			for(int e = 0; e <= outputWeights[0].length; e ++) {
				outputWeights[i][e] = getRandomWeight();
			}
		}
	}
	
	private double getRandomWeight() {
		return (double) random.nextDouble() - 0.5;
	}
	
	private double[] startNetwork(double[] networkInput) {
		
		input = new double[inputNeurons + 1];
		hidden = new double[hiddenNeurons.length][getMaxHiddenNeurons() + 1];
		output = new double[outputNeurons + 1];
		//bias:
		output[0] = 1;
		//Hidden Bias:
		for(int i = 0; i < hiddenNeurons.length; i ++) {
			hidden[i][0] = 1;
		}
		input[0] = 1;
		
		//Einen nach hinten wegen bias
		for(int i = 0; i < networkInput.length; i ++) {
			input[i + 1] = networkInput[i];
		}
		
		//NEU
		for(int i = 0; i < hiddenNeurons.length; i ++) {
			//Connected to input neurons
			if(i == 0) {
				for(int j = 1; j <= hidden[i].length; j ++) {
					hidden[i][j] = 0;
					for(int e = 0; e < input.length; e ++) {
						hidden[i][j] += input[e] * hiddenWeights[i][j][e];
					}
					hidden[i][j] = sigmoide(hidden[i][j]);
				}
			}
			//Connected to other hidden neurons
			else {
				for(int j = 1; j <= hidden[i].length; j ++) {
					hidden[i][j] = 0;
					for(int e = 0; e < hidden[i - 1].length; e ++) {
						hidden[i][j] += hidden[i - 1][e] * hiddenWeights[i][j][e];
					}
				}
			}
		}
		
		for(int i = 1; i <= outputNeurons; i ++) {
			output[i] = 0.0;
			for(int e = 0; e <= hidden[hidden.length - 1].length; e ++) {
				output[i] += (hidden[hidden.length - 1][e] * outputWeights[i][e]);
			}
			output[i] = sigmoide(output[i]);
		}
		return output;
	}
	
	private double sigmoide(double sum) {
		return (double) (1.0 / (1.0 + Math.exp(- sum)));
	}
	
	private void backpropagation(double[] desiredOutput) {
        double[] errorL2 = new double[outputNeurons + 1];
        double[][] errorL1 = new double[hiddenNeurons.length][getMaxHiddenNeurons()];//
        double Esum = 0.0;

        for (int i = 1; i <= outputNeurons; i++) {
            errorL2[i] = output[i] * (1.0 - output[i]) * (desiredOutput[i - 1] - output[i]);
        }

        for (int i = 0; i <= hiddenNeurons; i++) {
            for (int j = 1; j <= outputNeurons; j++) {
                Esum += outputWeights[j][i] * errorL2[j];
            }

            errorL1[i] = hidden[i] * (1.0 - hidden[i]) * Esum;
            Esum = 0.0;
        }

        //_______________________________________________________________________________________________________|
        
        for (int j = 1; j <= outputNeurons; j++) {
            for (int i = 0; i <= hiddenNeurons; i++) {
                outputWeights[j][i] += learningrate * errorL2[j] * hidden[i];
            }
        }

        for (int j = 1; j <= hiddenNeurons; j++) {
            for (int i = 0; i <= inputNeurons; i++) {
                hiddenWeights[j][i] += learningrate * errorL1[j] * input[i];
            }
        }
    }

	public double getLearningrate() {
		return learningrate;
	}

	public void setLearningrate(double learningrate) {
		this.learningrate = learningrate;
	}
}
