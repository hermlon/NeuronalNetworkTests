package filternetmutate;

public class MLP {

    public int nInputs, nHidden, nOutput;
    public double[] input, hidden, output;

    public double[][] weightL1, weigthL2;
    public double learningRate = 0.5;

    public double errorlevel = 0;

    public MLP(int nInput, int nHidden, int nOutput) {

        this.nInputs = nInput;
        this.nHidden = nHidden;
        this.nOutput = nOutput;

        input = new double[nInput + 1];
        hidden = new double[nHidden + 1];
        output = new double[nOutput + 1];

        weightL1 = new double[nHidden + 1][nInput + 1];
        weigthL2 = new double[nOutput + 1][nHidden + 1];

        generateRandomWeights();
    }

    public MLP(MLP net) {
        this.nInputs = net.nInputs;
        this.nHidden = net.nHidden;
        this.nOutput = net.nOutput;

        input = new double[net.nInputs + 1];
        hidden = new double[net.nHidden + 1];
        output = new double[net.nOutput + 1];

        weightL1 = new double[net.nHidden + 1][net.nInputs + 1];
        weigthL2 = new double[net.nOutput + 1][net.nHidden + 1];

        this.learningRate = net.learningRate;

        for (int j = 1; j <= nHidden; j++) {
            for (int i = 0; i <= nInputs; i++) {
                weightL1[j][i] = net.weightL1[j][i];
            }
        }

        for (int j = 1; j <= nOutput; j++) {
            for (int i = 0; i <= nHidden; i++) {
                weigthL2[j][i] = net.weigthL2[j][i];
            }
        }
    }

    public void mutate(double rate) {
        for (int j = 1; j <= nHidden; j++) {
            for (int i = 0; i <= nInputs; i++) {
                if (Math.random() < rate) {
                    weightL1[j][i] += (Math.random() * 2 - 1) * weightL1[j][i] * learningRate;
                }
            }
        }

        for (int j = 1; j <= nOutput; j++) {
            for (int i = 0; i <= nHidden; i++) {
                if (Math.random() < rate) {
                    weigthL2[j][i] += (Math.random() * 2 - 1) * weigthL2[j][i] * learningRate;
                }
            }
        }
    }

    public void setLearningRate(double lr) {
        learningRate = lr;
    }

    private void generateRandomWeights() {

        for (int j = 1; j <= nHidden; j++) {
            for (int i = 0; i <= nInputs; i++) {
                weightL1[j][i] = Math.random() - 0.5;
            }
        }

        for (int j = 1; j <= nOutput; j++) {
            for (int i = 0; i <= nHidden; i++) {
                weigthL2[j][i] = Math.random() - 0.5;
            }
        }
    }

    public double[] train(double[] pattern, double[] desiredOutput) {
        double[] output = passNet(pattern);
        backpropagation(desiredOutput);

        return output;
    }

    public double geterrorlevel(double[] pattern, double[] desiredOutput) {
        double[] output = passNet(pattern);

        for (int i = 1; i <= nOutput; i++) {
            double akkv = desiredOutput[i - 1] - output[i];
            errorlevel += (akkv < 0 ? -akkv : akkv);
        }

        return errorlevel;
    }

    public double[] passNet(double[] pattern) {

        for (int i = 0; i < nInputs; i++) {
            input[i + 1] = pattern[i];
        }

        input[0] = 1.0;
        hidden[0] = 1.0;

        for (int j = 1; j <= nHidden; j++) {
            hidden[j] = 0.0;
            for (int i = 0; i <= nInputs; i++) {
                hidden[j] += weightL1[j][i] * input[i];
            }
            hidden[j] = 1.0 / (1.0 + Math.exp(-hidden[j]));
        }

        for (int j = 1; j <= nOutput; j++) {
            output[j] = 0.0;
            for (int i = 0; i <= nHidden; i++) {
                output[j] += weigthL2[j][i] * hidden[i];
            }
            output[j] = 1.0 / (1.0 + Math.exp(-output[j]));
        }

        return output;
    }

    private void backpropagation(double[] desiredOutput) {

        double[] errorL2 = new double[nOutput + 1];
        double[] errorL1 = new double[nHidden + 1];
        double Esum = 0.0;

        for (int i = 1; i <= nOutput; i++) {
            errorL2[i] = output[i] * (1.0 - output[i]) * (desiredOutput[i - 1] - output[i]);
        }

        for (int i = 0; i <= nHidden; i++) {
            for (int j = 1; j <= nOutput; j++) {
                Esum += weigthL2[j][i] * errorL2[j];
            }

            errorL1[i] = hidden[i] * (1.0 - hidden[i]) * Esum;
            Esum = 0.0;
        }

        for (int j = 1; j <= nOutput; j++) {
            for (int i = 0; i <= nHidden; i++) {
                weigthL2[j][i] += learningRate * errorL2[j] * hidden[i];
            }
        }

        for (int j = 1; j <= nHidden; j++) {
            for (int i = 0; i <= nInputs; i++) {
                weightL1[j][i] += learningRate * errorL1[j] * input[i];
            }
        }
    }

}
