package net.hermlon.picturefilter.main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.imageio.ImageIO;

public class FilterTest {

	private PictureFilter filter;
	private File inputTrain = new File("E:/filter/cat.png");
	private File outputTrain = new File("E:/filter/Filter/gold.png");
	private File outputDir = new File("E:/pictureoutput/testoutput");
	private File inputPass = new File("E:/pictureoutput/katzegro�/cat.png");
	
	public FilterTest() {
		filter = new PictureFilter();
		filter.setGenerations(100);
		filter.train(inputTrain, outputTrain);
		
		BufferedImage[] img1 = {filter.pass(inputPass)};
		savePictures(img1);
	}
	
	private void savePictures(BufferedImage[] pictures) {
		for(int i = 0; i < pictures.length; i ++) {
			if(i % 10 == 0) {
				try {
					ImageIO.write(pictures[i], "png", new File(outputDir, getTimestamp() + "_gen_" + Integer.toString(i) + ".png"));
				}
				catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private String getTimestamp() {
		GregorianCalendar g = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddkkmmss");
		return sdf.format(g.getTime());
	}
}
