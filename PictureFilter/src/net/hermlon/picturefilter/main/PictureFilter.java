package net.hermlon.picturefilter.main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.hermlon.picturefilter.neuronalnetwork.NeuronalNetworkTrainer;

public class PictureFilter {
	
	private NeuronalNetworkTrainer networkTrainer;
	private int generations = 10;
	private int generation = 0;
	
	public PictureFilter() {
		networkTrainer = new NeuronalNetworkTrainer(1); 
	}
	
	public void train(File input, File expectedOutput) {
		try {
			BufferedImage i = ImageIO.read(input);
			BufferedImage o = ImageIO.read(expectedOutput);
			trainPictures(i, o);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public BufferedImage pass(File input) {
		try {
			BufferedImage i = ImageIO.read(input);
			BufferedImage[] image = {i};
			return networkTrainer.passNetwork(image);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private void trainPictures(BufferedImage input, BufferedImage expectedoutput) {
		BufferedImage[] i = {input};
		for(generation = 0; generation < generations; generation ++) {
			double p = 100d / generations * generation;
			System.out.println((int) p + "%");
			networkTrainer.trainNetwork(i, expectedoutput);
		}
	}

	public int getGenerations() {
		return generations;
	}

	public void setGenerations(int generations) {
		this.generations = generations;
	}
}
