package net.hermlon.endlessloop.elements;

public class Element {
	
	public static final int TYPE_EMPTY = 0;
	public static final int TYPE_LINE = 1;
	public static final int TYPE_CIRCLE = 2;
	public static final int TYPE_CURVE = 3;
	public static final int TYPE_SQUARE = 4;
	public static final int TYPE_V = 5;
	
	private int type;
	private int rotation;
	
	public Element(int type, int rotation) {
		this.type = type;
		this.rotation = rotation;
	}
	
	public int getRotation() {
		return rotation;
	}
	public void setRotation(int rotation) {
		this.rotation = rotation;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	
	public String getDebugCharacter() {
		switch(type) {
		case 0: return "X";
		case 1: switch(rotation) {
				case 1: return "|";
				case 2: return "-";
				case 3: return "|";
				case 4: return "-";
				}
		case 2: switch(rotation) {
				case 1: return "ᑲ";
				case 2: return "ᓂ";
				case 3: return "ᑫ";
				case 4: return "ᓀ";
		}
		case 3: switch(rotation) {
				case 1: return "ᒪ";
				case 2: return "ᒥ";
				case 3: return "ᒣ";
				case 4: return "ᒧ";
		}
		case 4: switch(rotation) {
				case 1: return "༓";
				case 2: return "༓";
				case 3: return "༓";
				case 4: return "༓";
		}
		case 5: switch(rotation) {
				case 1: return "v";
				case 2: return "<";
				case 3: return "ᄉ";
				case 4: return ">";
		}
		}
		return "error";
	}

}
