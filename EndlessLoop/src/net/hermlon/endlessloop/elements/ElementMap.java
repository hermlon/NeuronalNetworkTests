package net.hermlon.endlessloop.elements;

public class ElementMap {
	
	private int width;
	private int height;
	private Element[][] content;

	public ElementMap(int width, int height) {
		this.width = width;
		this.height = height;
		content = new Element[width][height];
	}
	
	public ElementMap(Element[][] content) {
		this.width = widthFromContent(content);
		this.height = heightFromContent(content);
		this.content = content;
	}
	
	public void setElement(Element element, int x, int y) {
		content[x][y] = element;
	}
	
	public Element getElement(int x, int y) {
		return content[x][y];
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
	
	private int heightFromContent(Element[][] c) {
		int maxy = 0;
		for(int i = 0; i < c.length; i ++) {
			if(c[i].length > maxy) {
				maxy = c[i].length;
			}
		}
		return maxy;
	}
	
	private int widthFromContent(Element[][] c) {
		return c.length;
	}
	
	public void print() {
		String n = "------MAP-----\n";
		for(int i = 0; i < getHeight(); i ++) {
			for(int j = 0; j < getWidth(); j ++) {
				//System.out.println("i:" + i + ", j:" + j);
				//System.out.println("w:" + width + ", h:" + height);
				n = n + getElement(j, i).getDebugCharacter();
			}
			n = n + "\n";
		}
		System.out.println(n);
	}
}
