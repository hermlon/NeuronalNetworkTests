package net.hermlon.endlessloop.manager;

import net.hermlon.endlessloop.elements.Element;
import net.hermlon.endlessloop.elements.ElementMap;
import net.hermlon.endlessloop.neuronalnetwork.NeuronalNetwork;

public class EndlessLoopManager {
	
	private NeuronalNetwork neuronalNetwork;
	
	public EndlessLoopManager() {
		neuronalNetwork = new NeuronalNetwork(18, 10, 1);
	}
	
	public ElementMap trainNetwork(ElementMap inputMap, ElementMap outputMap) {
		ElementMap output = new ElementMap(inputMap.getWidth() - 2, inputMap.getHeight() - 2);
		for(int i = 0; i < inputMap.getWidth() - 2; i ++) {
			for(int j = 0; j < inputMap.getHeight() - 2; j ++) {
				double[] expectedOutputRotation = {rotationToNetworkNumber(getExpectedRotation(outputMap, i, j))};
				double[] result = neuronalNetwork.train(getElementNumbers(inputMap, i, j), expectedOutputRotation);
				output.setElement(new Element(inputMap.getElement(i + 1, j + 1).getType(), rotationToRealNumber(result[0])), i, j);
				System.out.println("Element: " + (i + 1) + ";" + (j + 1) + " -> Type: " + inputMap.getElement(i + 1, j + 1).getType() + " Rotation: " + rotationToRealNumber(result[0]));
			}
		}
		return output;
	}
	
	public double getExpectedRotation(ElementMap map, int x, int y) {
		return map.getElement(x + 1, y + 1).getRotation();
	}
	
	public double[] getElementNumbers(ElementMap map, int x, int y) {
		double[] numbers = new double[3 * 3 * 2];
		int pos = 0;
		//Types
		for(int i = 0; i < 3; i ++) {
			for(int j = 0; j < 3; j ++) {
				Element e = map.getElement(x + i, y + j);
				numbers[pos] = typeToNetworkNumber(e.getType());
				pos ++;
			}
		}
		//Rotations
		for(int i = 0; i < 3; i ++) {
			for(int j = 0; j < 3; j ++) {
				Element e = map.getElement(x + i, y + j);
				numbers[pos] = rotationToNetworkNumber(e.getRotation());
				pos ++;
			}
		}
		return numbers;
	}
	
	public double rotationToNetworkNumber(double realNumber) {
		return realNumber / 4;
	}
	
	public int rotationToRealNumber(double networkNumber) {
		return (int) networkNumber * 4;
	}
	
	public double typeToNetworkNumber(double realNumber) {
		return realNumber / 5;
	}
	
	public int typeToRealNumber(double networkNumber) {
		return (int) networkNumber * 5;
	}
}
