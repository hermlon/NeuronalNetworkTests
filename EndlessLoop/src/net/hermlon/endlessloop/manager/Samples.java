package net.hermlon.endlessloop.manager;

import net.hermlon.endlessloop.elements.Element;

public class Samples {

	public static Element[][] input1 = {
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_CURVE, 1), new Element(Element.TYPE_CIRCLE, 1), new Element(Element.TYPE_CURVE, 1), new Element(Element.TYPE_CIRCLE, 1), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_LINE, 2), new Element(Element.TYPE_CIRCLE, 1), new Element(Element.TYPE_V, 4), new Element(Element.TYPE_CURVE, 4), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_V, 2), new Element(Element.TYPE_CIRCLE, 4), new Element(Element.TYPE_CIRCLE, 2), new Element(Element.TYPE_V, 3), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_CURVE, 4), new Element(Element.TYPE_CIRCLE, 3), new Element(Element.TYPE_CIRCLE, 2), new Element(Element.TYPE_V, 1), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_CIRCLE, 4), new Element(Element.TYPE_LINE, 1), new Element(Element.TYPE_CURVE, 1), new Element(Element.TYPE_LINE, 2), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_CURVE, 2), new Element(Element.TYPE_CURVE, 1), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0)}
	};
	
	public static Element[][] output1 = {
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_CURVE, 2), new Element(Element.TYPE_CIRCLE, 4), new Element(Element.TYPE_CURVE, 2), new Element(Element.TYPE_CIRCLE, 4), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_LINE, 1), new Element(Element.TYPE_CIRCLE, 2), new Element(Element.TYPE_V, 3), new Element(Element.TYPE_CURVE, 3), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_V, 4), new Element(Element.TYPE_CIRCLE, 4), new Element(Element.TYPE_CIRCLE, 2), new Element(Element.TYPE_V, 2), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_CURVE, 1), new Element(Element.TYPE_CIRCLE, 4), new Element(Element.TYPE_CIRCLE, 2), new Element(Element.TYPE_V, 2), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_CIRCLE, 2), new Element(Element.TYPE_LINE, 2), new Element(Element.TYPE_CURVE, 3), new Element(Element.TYPE_LINE, 1), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_CURVE, 1), new Element(Element.TYPE_CURVE, 4), new Element(Element.TYPE_EMPTY, 0)},
			{new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0), new Element(Element.TYPE_EMPTY, 0)}
			};
}
