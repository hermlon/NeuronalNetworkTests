package net.hermlon.endlessloop.manager;

import net.hermlon.endlessloop.elements.ElementMap;

public class EndlessLoopTest {

	private int generation;
	private int generations = 10;
	private EndlessLoopManager endlessLoopManager;
	
	public EndlessLoopTest() {
		endlessLoopManager = new EndlessLoopManager();
	}
	
	public void train(ElementMap inputMap, ElementMap outputMap, int generations) {
		for(int generation = 0; generation < generations; generation ++) {
			System.out.println("-----------" + generation + "---------");
			ElementMap map = endlessLoopManager.trainNetwork(inputMap, outputMap);
			if(generation == generations - 1) {
				map.print();
			}
		}
	}
	
	public int getGenerations() {
		return generations;
	}
	public void setGenerations(int generations) {
		this.generations = generations;
	}
	
}
