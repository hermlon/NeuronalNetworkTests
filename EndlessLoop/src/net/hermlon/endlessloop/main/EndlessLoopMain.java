package net.hermlon.endlessloop.main;

import java.io.File;

import net.hermlon.endlessloop.elements.ElementMap;
import net.hermlon.endlessloop.manager.EndlessLoopManager;
import net.hermlon.endlessloop.manager.EndlessLoopTest;
import net.hermlon.endlessloop.manager.Samples;
import net.hermlon.endlessloop.picture.PictureScanner;

public class EndlessLoopMain {

	public static void main(String[] args) {
		
		//EndlessLoopTest endlessLoopTest = new EndlessLoopTest();
		//endlessLoopTest.train(new ElementMap(Samples.input1), new ElementMap(Samples.output1), 10);
		/*
		ElementMap out = new ElementMap(Samples.input1);
		out.print();*/
		new PictureScanner().scanPicture(new File("G:/Screenshots/Screenshot_2016-07-16-18-04-26.png"));
	}

}
