package net.hermlon.endlessloop.picture;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.hermlon.endlessloop.elements.Element;
import net.hermlon.endlessloop.elements.ElementMap;

public class PictureScanner {
	
	public static final int diameter = 72;
	public static final int xoffset = 96;
	public static final int yoffset = 60;
	public static final int raster = 192;

	private BufferedImage image;
	private int[] rasterPosition;
	private int firstX;
	private int firstY;
	private ElementMap map;
	
	public ElementMap scanPicture(File picture) {
		try {
			image = ImageIO.read(picture);
			rasterPosition = getRasterStartPosition();
			map = new ElementMap(getWidth(), getHeight());
			fillElements();
			System.out.println(map.getWidth() + "x" + map.getHeight());
			map.print();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return map;
	}
	
	private void fillElements() {
		System.out.println(firstX + "|" + firstY);
		int imageX = firstX;
		int imageY = firstY;
		for(int x = 0; x < map.getWidth(); x ++) {
			imageY = firstY;
			for(int y = 0; y < map.getHeight(); y ++) {
				System.out.println(y);
				int e[] = getFromPixels(imageX, imageY);
				map.setElement(new Element(e[0],  e[1]), x, y);
				imageY += (raster + 1);
			}
			imageX += (raster + 1);
		}
	}
	
	private int[] getFromPixels(int x, int y) {
		int e[] = new int[2];
		if(isCleanArea(x, y, x + raster, y + raster)) {
			e[0] = 0;
			e[1] = 0;
			return e;
		}
		boolean c[] = getConnections(x, y);
		//Circle
		if(c[0] & !c[1] & !c[2] & !c[3]) {
			e[0] = Element.TYPE_CIRCLE;
			e[1] = 1;
			return e;
		}
		if(!c[0] & c[1] & !c[2] & !c[3]) {
			e[0] = Element.TYPE_CIRCLE;
			e[1] = 2;
			return e;
		}
		if(!c[0] & !c[1] & c[2] & !c[3]) {
			e[0] = Element.TYPE_CIRCLE;
			e[1] = 3;
			return e;
		}
		if(!c[0] & !c[1] & !c[2] & c[3]) {
			e[0] = Element.TYPE_CIRCLE;
			e[1] = 4;
			return e;
		}
		//Line
		if(c[0] & !c[1] & c[2] & !c[3]) {
			e[0] = Element.TYPE_LINE;
			e[1] = 1;
			return e;
		}
		if(!c[0] & c[1] & !c[2] & c[3]) {
			e[0] = Element.TYPE_LINE;
			e[1] = 2;
			return e;
		}
		//Curve
		if(c[0] & c[1] & !c[2] & !c[3]) {
			e[0] = Element.TYPE_CURVE;
			e[1] = 1;
			return e;
		}
		if(!c[0] & c[1] & c[2] & !c[3]) {
			e[0] = Element.TYPE_CURVE;
			e[1] = 2;
			return e;
		}
		if(!c[0] & !c[1] & c[2] & c[3]) {
			e[0] = Element.TYPE_CURVE;
			e[1] = 3;
			return e;
		}
		if(c[0] & !c[1] & !c[2] & c[3]) {
			e[0] = Element.TYPE_CURVE;
			e[1] = 4;
			return e;
		}
		//V
		if(!c[0] & c[1] & c[2] & c[3]) {
			e[0] = Element.TYPE_V;
			e[1] = 1;
			return e;
		}
		if(c[0] & !c[1] & c[2] & c[3]) {
			e[0] = Element.TYPE_V;
			e[1] = 2;
			return e;
		}
		if(c[0] & c[1] & !c[2] & c[3]) {
			e[0] = Element.TYPE_V;
			e[1] = 3;
			return e;
		}
		if(c[0] & c[1] & c[2] & !c[3]) {
			e[0] = Element.TYPE_V;
			e[1] = 4;
			return e;
		}
		//Square
		if(c[0] & c[1] & c[2] & c[3]) {
			e[0] = Element.TYPE_SQUARE;
			e[1] = 1;
			return e;
		}
		System.out.println("Error: Could not detect shape");
		return null;
	}
	
	private boolean[] getConnections(int x, int y) {
		int s = 83;
		boolean connections[] = new boolean [4];
		if(image.getRGB(x + s, y) != image.getRGB(x + s + 1, y)) {
			connections[0] = true;
		}
		if(image.getRGB(x + raster, y + s) != image.getRGB(x + raster, y + s + 1)) {
			connections[1] = true;
		}
		if(image.getRGB(x + s, y + raster) != image.getRGB(x + s + 1, y + raster)) {
			connections[2] = true;
		}
		if(image.getRGB(x, y + s) != image.getRGB(x, y + s + 1)) {
			connections[3] = true;
		}
		System.out.println(x + "|" + y + ": " + connections[0] + connections[1] + connections[2] + connections[3]);
		return connections;
	}
	
	private int[] getRasterStartPosition() {
		int[] pos = new int[2];
		for(int i = getRadius() + 1; i < image.getWidth() - getRadius() - 1; i ++) {
			for(int j = 0; j < image.getHeight() - getDiameter() - 1; j ++) {
				if(image.getRGB(i, j) != image.getRGB(i, j + 1) // oben
				   && image.getRGB(i, j + diameter) != image.getRGB(i, j + diameter + 1) // unten
				   && image.getRGB(i - getRadius(), j + getRadius()) != (image.getRGB(i - getRadius() + 1, j + getRadius())) // links
				   && image.getRGB(i + getRadius(), j + getRadius()) != (image.getRGB(i + getRadius() + 1, j + getRadius())) // rechts
				) {
						System.out.println("Circle Position: " + i + "|" + j);
						pos[0] = i - xoffset;
						pos[1] = j - yoffset;
						System.out.println("Raster Position: " + pos[0] + "|" + pos[1]);
						return pos;
				}
				/*if(i == 431 && j == 571) {
					System.out.println(i + "|" + j + "  Oben 1:" + image.getRGB(i, j));
					System.out.println(i + "|" + (j+1) + "  Oben 2:" + image.getRGB(i, j + 1));
					System.out.println(i + "|" + (j+ diameter) + "  Unten 1:" + image.getRGB(i, j + diameter));
					System.out.println(i + "|" + (j+ diameter+1) + "  Unten 2:" + image.getRGB(i, j + diameter + 1));
					System.out.println((i-getRadius()) + "|" + (j+getRadius()) + "  Links 1:" + image.getRGB(i - getRadius(), j + getRadius()));
					System.out.println((i-getRadius()-1) + "|" + (j+getRadius()) + "  Links 2:" + image.getRGB(i - getRadius() - 1, j + getRadius()));
					System.out.println((i+getRadius()) + "|" + (j+getRadius()) + "  Rechts 1:" + image.getRGB(i + getRadius(), j + getRadius()));
					System.out.println((i+getRadius()+1) + "|" + (j+getRadius()) + "  Rechts 2:" + image.getRGB(i + getRadius() + 1, j + getRadius()));
				}*/
			}
		}
		return pos;
	}
	
	private int getHeight() {
		boolean foundTop = false;
		boolean foundBottom = false;
		int height = 0;
		for(int y = rasterPosition[1]; !foundTop; y -= (raster + 1)) {
			if(!(y < 0 - raster)) {
				height ++;
				if(isCleanArea(0, y, image.getWidth(), y + raster)) {
					foundTop = true;
					firstY = y;
					break;
				}
			}
			else {
				System.out.println("Error: could not find top end");
				break;
			}
		}
		for(int y = rasterPosition[1] + raster + 1; !foundBottom; y += (raster /*+ 1*/)) {
			if(!(y > image.getHeight() + raster)) {
				height ++;
				if(isCleanArea(0, y, image.getWidth(), y + raster)) {
					foundBottom = true;
					break;
				}
			}
			else {
				System.out.println("Error: could not find bottom end");
				break;
			}
		}
		return height;
	}
	
	private int getWidth() {
		boolean foundLeft = false;
		boolean foundRight = false;
		int width = 0;
		for(int x = rasterPosition[0]; !foundLeft; x -= (raster + 1)) {
			if(!(x < 0 - raster)) {
				width ++;
				if(isCleanArea(x, 0, x + raster, image.getHeight())) {
					foundLeft = true;
					firstX = x;
					break;
				}
			}
			else {
				System.out.println("Error: could not find left end");
				break;
			}
		}
		
		for(int x = rasterPosition[0] + raster + 1; !foundRight; x += (raster/* + 1*/)) {
			if(!(x > image.getWidth() + raster)) {
				width ++;
				if(isCleanArea(x, 0, x + raster, image.getHeight())) {
					foundRight = true;
					break;
				}
			}
			else {
				System.out.println("Error: could not find right end");
				break;
			}
		}
		return width;
	}
	
	private boolean isCleanArea(int startx, int starty, int endx, int endy) {
		if(startx < 0) {startx = 0;System.out.println("Alarm1");}
		if(starty < 0) {starty = 0;System.out.println("Alarm2");}
		if(endx > image.getWidth()) {endx = image.getWidth();System.out.println("Alarm3");}
		if(endy > image.getHeight()) {endy = image.getHeight();System.out.println("Alarm4");}
		
		int lastColor = image.getRGB(startx, starty);
		for(int i = startx; i < endx; i ++) {
			for(int j = starty; j < endy; j ++) {
				if(lastColor != image.getRGB(i, j)) {
					return false;
				}
			}
		}
		System.out.println("Clean Area");
		return true;
	}
	
	private static int getDiameter() {
		return diameter;
	}
	
	private static int getRadius() {
		return diameter / 2;
	}
}
